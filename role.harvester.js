
const HARVESTING = "HARVESTING"
const RETURNING = "RETURNING"
const UPGRADE_CONTROLLER = "UPGRADE_CONTROLLER"

const memoryTemplate = {
    currentState: HARVESTING,
}

function isNotFull(harvester) {
    return harvester.store[RESOURCE_ENERGY] < harvester.store.getCapacity([RESOURCE_ENERGY]);
}

function harvest(harvester) {
    if (isNotFull(harvester)) {
        const target = harvester.pos.findClosestByRange(FIND_SOURCES_ACTIVE);
        if (target) {
            if (harvester.harvest(target) === ERR_NOT_IN_RANGE) {
                harvester.moveTo(target);
            }
        }
    } else {
        harvester.memory.state.currentState = RETURNING;
    }
}

function returnResources(harvester) {
    if (harvester.store[RESOURCE_ENERGY] !== 0) {
        const transfer = harvester.transfer(Game.spawns["MasterBase"], RESOURCE_ENERGY);
        if (transfer === ERR_NOT_IN_RANGE) {
            harvester.moveTo(Game.spawns["MasterBase"].pos);
            console.log("MOVING BITCH")
        } else if (transfer === ERR_FULL) {
            if (isNotFull(harvester)) {
                console.log(isNotFull(harvester))
                console.log("IT FULL");
                harvester.memory.state.currentState = HARVESTING;
            } else {
                harvester.memory.state.currentState = UPGRADE_CONTROLLER;
            }
        } else {
            console.log(`Invalid target: ${transfer}` );
        }
    } else {
        harvester.memory.state.currentState = HARVESTING;
    }

}

function upgradeController(harvester) {
    const controller = harvester.room.controller
    //console.log(JSON.stringify(controller));
    if (harvester.store[RESOURCE_ENERGY] !== 0) {
        const upgrade = harvester.upgradeController(controller);
        if (upgrade === ERR_NOT_IN_RANGE) {
            harvester.moveTo(controller);
            console.log("I move bitch")
        } else if (upgrade < 0) {
            console.log(`Errored out of upgrading controller. Return to return resoyurces: ${upgrade}`)
        }
    } else {
        harvester.memory.state.currentState = HARVESTING;
    }
}

module.exports = {
    runHarvester(harvester) {

        if (harvester.memory.state === undefined) {
            harvester.memory.state = Object.assign({}, memoryTemplate);
        }

        //console.log(JSON.stringify(harvester.store[RESOURCE_ENERGY]));
        if (harvester.memory.state.currentState === HARVESTING) {
            harvest(harvester);
        } else if (harvester.memory.state.currentState === RETURNING) {
            returnResources(harvester);
        } else if (harvester.memory.state.currentState === UPGRADE_CONTROLLER) {
            upgradeController(harvester);
        }
    }
}