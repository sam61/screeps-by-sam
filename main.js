const generator = require('role.generator');
const harvester = require('role.harvester');



module.exports.loop = function () {
    //console.log(JSON.stringify(Game.spawns["MasterBase"]))
    const initCreep = Game.creeps["InitialSpawner"];
    if (Object.keys(Game.creeps).length === 0){
        generator.noCreeps();
    }
    if (initCreep !== undefined) {
        harvester.runHarvester(initCreep);
    }

}